package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {
    @Override
    public long sum(List<Long> list) {
        if (list.isEmpty()){
            return 0;
        } else if (list.size() == 1) {
            return list.get(0);
        } else { 
            int subListSyntaxIndex = list.size();
            return list.get(0) + sum(list.subList(1, subListSyntaxIndex));
        }
    }
}
