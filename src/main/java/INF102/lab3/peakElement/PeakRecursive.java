package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    
    @Override
    public int peakElement(List<Integer> numbers) {
        int n = numbers.size();
        if (numbers.isEmpty()) {
            throw new IllegalArgumentException();
        }
        if (n == 1){
            return numbers.get(0);
        } else {
            if (numbers.get(0) >= numbers.get(1)){
                return numbers.get(0);
            } else if (numbers.get(n-1) >= numbers.get(n-2)){
                return numbers.get(n-1);
            } else {
                return peakElement(numbers.subList(1, n-1));
            }
        }
        }


}
