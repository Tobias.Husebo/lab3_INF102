package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int lowerb = number.getLowerbound();
        int upperb = number.getUpperbound();
        int n = (upperb + lowerb)/2;

        while (true) {
            int guessQuery = number.guess(n);
            //System.out.println(n);
            if (guessQuery == 0) {
                return n;
            } else if (guessQuery == 1) {
                upperb = n-1;
            } else {
                lowerb = n+1;
            }
            n = (upperb + lowerb)/2;
        }
    }

}
